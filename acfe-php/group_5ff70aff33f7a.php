<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5ff70aff33f7a',
	'title' => 'ColorSelection',
	'fields' => array(
		array(
			'key' => 'field_5ff70b02b41f8',
			'label' => 'Color Selection',
			'name' => 'color_selection',
			'aria-label' => '',
			'type' => 'select',
			'instructions' => 'Please select a background-color',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'bg-transparent' => 'bg-transparent',
				'bg-red' => 'bg-red',
				'bg-blue' => 'bg-blue',
				'bg-rose' => 'bg-rose',
				'bg-yellow' => 'bg-yellow',
				'bg-green' => 'bg-green',
				'bg-smoke' => 'bg-smoke',
			),
			'default_value' => 'bg-yellow',
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'translations' => 'copy_once',
			'ajax' => 0,
			'placeholder' => '',
			'allow_custom' => 0,
			'search_placeholder' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'positions',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
	'acfe_display_title' => '',
	'acfe_autosync' => array(
		0 => 'php',
		1 => 'json',
	),
	'acfe_form' => 0,
	'acfe_meta' => '',
	'acfe_note' => '',
	'modified' => 1679505016,
));

endif;