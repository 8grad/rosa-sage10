<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_60364fe1f3506',
	'title' => 'soundcloudplayer',
	'fields' => array(
		array(
			'key' => 'field_60364feb80597',
			'label' => 'Track ID',
			'name' => 'id',
			'aria-label' => '',
			'type' => 'number',
			'instructions' => 'Die ID des Tracks findest Du folgendermaßen: api.soundcloud.com/tracks/XXXXXXXXX&color=',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
			'translations' => 'copy_once',
		),
		array(
			'key' => 'field_60364ffb80598',
			'label' => 'Beschreibung des Tracks',
			'name' => 'footnote',
			'aria-label' => '',
			'type' => 'text',
			'instructions' => 'z.B.: Auszug aus einem Brief Rosa Luxemburgs an XXX im September 1904, gelesen von XX XX',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'translations' => 'translate',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'material',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
	'acfe_display_title' => '',
	'acfe_autosync' => array(
		0 => 'php',
		1 => 'json',
	),
	'acfe_form' => 0,
	'acfe_meta' => '',
	'acfe_note' => '',
	'modified' => 1679505183,
));

endif;