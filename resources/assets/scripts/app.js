/**
 * External Dependencies
 */

import 'jquery';

import '../../../resources/views/components/custom-post-cols/custom-post-cols';
import '../../../resources/views/components/custom-post-decisions/custom-post-decisions';
import '../../../resources/views/components/scroll-navigation/scroll-navigation';
import '../../../resources/views/components/scroll-to-top/scroll-to-top';

import '../../../resources/views/components/debug/debug';
import '../../../resources/views/components/inline-image/inline-image';
import '../../../resources/views/components/figure-with-popover/figure-with-popover';
import '../../../resources/views/components/footnotes/footnotes';
import '../../../resources/views/components/page-search/page-search';
