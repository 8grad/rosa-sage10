<?php
  $notification = new WP_Query([
      'post_type' => 'notifications',
      'orderby' => 'date',
      'order' => 'DEC',
      'posts_per_page' => 1,
    ]
  );
?>

<section>
  @posts($notification)
  <a class="group outline-none" href="@field('url')" title="@field('title')" target="_blank">
  <span class="flex items-center w-full justify-center">
    <span class="block bg-black text-white w-40 h-40 shadow-default rounded-full font-sans flex flex-col items-center justify-center transform scale-75 md:scale-75 xl:scale-150 -mr-40 -mb-64 md:-mr-20 md:-mb-20">
    <span class="block block flex-0 w-28 h-28 overflow-hidden transform -rotate-30 pt-2">
      <p class="outline-none m-0 leading-none whitespace-nowrap text-lg text-yellow tracking-wider pb-1">@field('title')</p>
      <p class="outline-none m-0 leading-none whitespace-nowrap text-lg text-white font-display tracking-wider pb-1">@field('date')</p>
      <p class="outline-none m-0 leading-none text-xs text-yellow tracking-wider">@field('description')</p>
    </span>
  </span>
  </span>
  </a>
  @endposts
</section>
