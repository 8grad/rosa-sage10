const scrollNavigation = (function scrollNavigation() {
  const scrollNavigationSelector = '[data-component="scroll-navigation"]';
  const observeSelector = '[data-role="observe-intersection"]';
  const stationSelector = '[data-role="station"]';
  const downSelector = '[data-role="move-down"]';
  const upSelector = '[data-role="move-up"]';
  const topSelector = '[data-role="move-top"]';

  const element = document.querySelector(scrollNavigationSelector);
  const station = document.querySelector(stationSelector);
  const down = document.querySelector(downSelector);
  const up = document.querySelector(upSelector);
  const top = document.querySelector(topSelector);

  const observedElements = document.querySelectorAll(observeSelector);
  const topDistance = 50;
  const scrollIntoView = require('scroll-into-view');

  const options = options || {
    threshold: 0.5,
  };

  if (element) {

    const scrollHandle = () => {
      let y = window.scrollY;

      if (y > topDistance) {
        element.classList.add('is-visible');
      } else {
        element.classList.remove('is-visible');
      }

      initInView();
    };

    const updateBar = (id) => {
      if (id) {
        top.setAttribute('href', '#' + id);

        station.innerHTML = id;
        let nextID = Number(id) + 1;
        let prevID = Number(id) - 1;

        if (nextID <= 10) {
          down.setAttribute('href', '#' + nextID);
          down.classList.add('text-yellow');
          down.classList.remove('text-gray-700', 'cursor-not-allowed');
        } else {
          down.classList.remove('text-yellow');
          down.classList.add('text-gray-700', 'cursor-not-allowed');
        }

        if (prevID >= 1) {
          up.setAttribute('href', '#' + prevID);
          up.classList.add('text-yellow');
          up.classList.remove('text-gray-700', 'cursor-not-allowed');
        } else {
          up.classList.remove('text-yellow');
          up.classList.add('text-gray-700', 'cursor-not-allowed');
        }
      }
    }

    const intoView = (el) => {
      // options: https://www.npmjs.com/package/scroll-into-view
      scrollIntoView(el, {
        cancellable: false,
        time: 1250,
        align: {
          top: 0,
          topOffset: 64,
        },
      });
    }

    const clickHandle = (event) => {
      event.preventDefault(); /* we need a safari work around for smooth scroll*/
      let hash = event.target.hash;
      let id = hash.substring(1);
      let linkTo = document.querySelector('[data-hash="' + id + '"]' );
      intoView(linkTo);
    };

    const isInView = (el) => {
      let box = el.getBoundingClientRect();
      return box.top < window.innerHeight - 50 && box.bottom >= 0;
    }

    const initInView = () => {
      observedElements.forEach((pattern) => {
        if (isInView(pattern)) {
          //console.log(pattern.getAttribute('data-hash'));
          updateNavigation(pattern.getAttribute('data-hash'));
        }
      });
    }


    const theID = async (id) => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (id) {
            resolve(id);
          }
        }, 100);
      })

    };

    const updateNavigation = (id) => {
      theID(id).then(function (value) {
        if (id) {
          updateBar(id);
        } else {
          updateBar(" … ");
        }
      });
    }


    /**
     * debounce scroll
     */
    const throttle = (fn, wait) => {
      var time = Date.now();
      return function () {
        if ((time + wait - Date.now()) < 0) {
          fn();
          time = Date.now();
        }
      }
    }

    const bindEvents = () => {
      window.addEventListener('scroll', throttle(scrollHandle, 250), false);
      down.addEventListener('click', clickHandle, false);
      up.addEventListener('click', clickHandle, false);
      top.addEventListener('click', clickHandle, false);
    };

    const init = () => {
      bindEvents();
    };

    init();
  }
  return scrollNavigation;
})
();

export default scrollNavigation;
