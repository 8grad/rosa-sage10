<div class="scroll-navigation shadow-default fixed z-10 bottom-0 mb-16 w-full h-0" data-component="scroll-navigation">
  <div class="grid grid-cols-12">
    <div class="col-span-12 md:col-start-4 md:col-end-13">
      <div class="flex items-center justify-center h-0">
        <div class="flex-0 bg-black rounded-full flex items-center font-sans" data-role="bar">
          <a data-role="move-down" aria-label="Move down" class="h-10 w-10 flex items-center justify-center mr-2 text-gray-300" href="#">@fas('arrow-down', 'pointer-events-none')</a>
          <a data-role="move-top" aria-label="Move to top" class="text-yellow" href="#"><?= __( 'Station', 'sage' ); ?> <span class="pointer-events-none inline-block w-6" data-role="station">1</span></a>
          <a data-role="move-up" aria-label="Move up" class="h-10 w-10  flex items-center justify-center text-gray-300" href="#">@fas('arrow-up', 'pointer-events-none')</a>
        </div>
      </div>
    </div>
  </div>
</div>
