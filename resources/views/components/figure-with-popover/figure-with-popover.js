import tippy, {roundArrow} from 'tippy.js';
import 'tippy.js/dist/svg-arrow.css';

//import screenIs from '../screens/screens';
import * as basicLightbox from 'basiclightbox'
import 'tippy.js/themes/light.css';

const figure = (function figure(obj) {
  const componentSelector = 'figure';
  const componentClassName = 'figure-with-popover';
  const componentImageClassName = 'figure-with-popover__image';
  const componentButtonClassName = 'figure-with-popover__close-button';
  const componentScaleButtonClassName = 'figure-with-popover__scale-button';
  const componentCaptionClassName = 'figure-with-popover__figcaption';

  /**
   * setup tippy instances
   */
  const initTippy = (elements) => {
    const tooltip = document.createElement('div');
    tippy(elements, {
      appendTo: document.body,
      theme: 'light',
      hideOnClick: true,
      trigger: 'click',
      allowHTML: true,
      content: tooltip,
      placement: 'bottom',
      zIndex: 11,
      interactive: true,
      maxWidth: false,
      animation: 'scale-extreme',
      inertia: true,
      arrow: roundArrow,
      popperOptions: {
        strategy: 'fixed',
        modifiers: [
          {
            name: 'flip',
            options: {
              fallbackPlacements: ['bottom', 'right'],
            },
          },
          {
            name: 'preventOverflow',
            options: {
              altAxis: true,
              tether: false,
            },
          },
        ],
      },

      onShow(instance) {
        /**
         * Build tippy DOM
         */
        let tooltipInner = document.createElement('figure');
        tooltipInner.classList.add(componentClassName);

        let image = new Image();
        image.width = 800;
        image.height = 200;
        image.src = instance.reference.children[0].src;
        image.classList.add(componentImageClassName);

        tooltipInner.appendChild(image);

        let caption = document.createElement('figcaption');
        let closeButton = document.createElement('button');
        let scaleButton = document.createElement('button');

        tooltipInner.appendChild(closeButton);

        closeButton.classList.add(componentButtonClassName);
        closeButton.innerHTML = "<i class='fas fa-times pointer-events-none'>";

        scaleButton.classList.add(componentScaleButtonClassName);
        scaleButton.innerHTML = "<i class='fas fa-expand-alt pointer-events-none'>";

        tooltipInner.appendChild(scaleButton);

        // has caption
        if (instance.reference.children.length >= 2) {
          caption.innerHTML = instance.reference.children[1].innerHTML;
          caption.classList.add(componentCaptionClassName);

          tooltipInner.appendChild(caption);
        }

        instance.setContent(tooltipInner);
        handleClose(instance, closeButton);
        handleScale(instance, scaleButton);
        handleCloseScroll(instance);
      },
    });
  }

  /**
   * handle tippy init
   */
  const initFigures = (el) => {
    if (el.length >= 1) {
      initTippy(el);
    }
  }

  /**
   * debounce scroll
   */
  const throttle = (fn, wait, instance) => {
    var time = Date.now();
    return function () {
      if ((time + wait - Date.now()) < 0) {
        fn(instance);
        time = Date.now();
      }
    }
  }

  /**
   * handle tippy close button
   */
  const handleClose = (instance, el) => {
    el.addEventListener('click', function () {
      instance.hide();
    })

    window.addEventListener('keydown', function (e) {
      if ((e.key == 'Escape' || e.key == 'Esc' || e.keyCode == 27) && (e.target.nodeName == 'BODY')) {
        e.preventDefault();
        instance.hide();
        return false;
      }
    }, true)
  }

  /**
   * handle inner tippy scale button
   */
  const handleScale = (instance, el) => {
    el.addEventListener('click', function () {
      basicLightbox.create('<img width="1400" height="900" src="' + instance.reference.children[0].src + '">').show()
    })
  }

  const hideTippyOnDistantScroll = (distance, start, end, instance) => {
    if (distance >= 60) {
      instance.hide();
    }
  }

  const scrollHandle = (instance) => {
    let start;
    let end;
    let distance;
    let isScrolling;

    // Set starting position
    if (!start) {
      start = window.pageYOffset;
    }

    // Clear our timeout throughout the scroll
    window.clearTimeout(isScrolling);

    // Set a timeout to run after scrolling ends
    isScrolling = setTimeout(function () {

      // Calculate distance
      end = window.pageYOffset;
      distance = end - start;

      // Run the callback
      hideTippyOnDistantScroll(distance, start, end, instance);

    }, 1000);

  }

  const handleCloseScroll = (instance) => {
    window.addEventListener('scroll', throttle(scrollHandle, 250, instance), false);
  }

  if (obj) {
    let figures = obj.entry.target.querySelectorAll(componentSelector);
    console.info(figures.length + ' fetched Figures in ' + obj.entry + ' found');
    initFigures(figures);

  } else {
    /* not called via fetch/axios */
    let figures = document.querySelectorAll(componentSelector);
    console.info(figures.length + ' global Figures found');
    initFigures(figures);

  }

  return figure; /** make sure, this IIFE function is callable */
})();

export default figure;
