const debug = (function debug() {
  const debugSelector = '[data-component="debug"]';
  const containerSelector = '[data-role="debug"]';
  const debugClass = 'debug';

  const element = document.querySelector(debugSelector);
  const container = document.querySelector(containerSelector);

  if (element) {
    const toggleClass = () => {
      container.classList.toggle(debugClass);
      document.cookie = element.checked === true ? 'mode=debug' : 'mode=';
    };

    const readCookie = () => {
      if (document.cookie.split(';').filter(function (item) {
        return item.indexOf('mode=debug') >= 0;
      }).length) {
        element.checked = true;
        container.classList.add(debugClass);
      }
    };

    const bindEvents = () => {
      element.addEventListener('change', toggleClass, false);
    };

    const init = () => {
      bindEvents();
      readCookie();
    };

    init();
  }
  return debug;
})();

export default debug;
