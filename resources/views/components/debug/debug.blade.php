<section class="fixed bottom-0 z-50 bg-black text-white font-mono px-1 hidden md:block">
  <div class="text-xs">
    <div class="w-full">
      <div style="font-size: 8px">
        <input class="" type="checkbox" data-component="debug" id="grid">
        <label class="cursor-pointer whitespace-nowrap" for="grid">⊞ show grid</label> | <?= wp_get_theme() ?> | PHP
        v. <?= phpversion() ?> | WP v. <?= get_bloginfo( 'version' );
        ?>
      </div>
    </div>
  </div>
</section>
