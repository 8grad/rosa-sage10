import figure from '../figure-with-popover/figure-with-popover';
import inlineImage from "../inline-image/inline-image";

const observeDecisions = (function () {
  const axios = require('axios');
  const componentSelector = '[data-component="decisions"]';
  const observeSelector = '[data-role="observe-intersection"]';
  const siteUrlSelector = '[data-url]';
  const jsonEndpoint = '/wp-json/wp/v2/';

  const element = document.querySelector(componentSelector);
  const options = options || {
    threshold: 0.25,
  };

  if (element) {
    let observedElements = element.querySelectorAll(observeSelector);

    let axiosRequest = (id, entry) => {
      let siteUrlElement = document.querySelector(siteUrlSelector);
      let siteUrl = siteUrlElement.dataset.url;
      let jsonUrl = siteUrl + jsonEndpoint + 'decisions/' + id;

      axios.get(jsonUrl)
        .then((response) => {

          // Pluck the .data array from the response.
          const post = response.data;
          let container = document.getElementById('decision__axios--' + post.id);
          container.innerHTML = post.content.rendered;
          container.classList.remove('opacity-0')

          /**
           * re-init plugins
           */
          inlineImage({entry});
          figure({entry});

        }).catch(function (error) {
        // handle error
        console.error(error);
      })
        .then(function () {
          //  always executed
        });
    };

    let callback = (entries) => {
      entries.forEach(entry => {
        if (entry.intersectionRatio > 0) {
          let id = entry.target.dataset.id;
          console.log(id);

          //axiosRequest(id, entry);
          observer.unobserve(entry.target);
        }
      });
    };

    let observer = new IntersectionObserver(callback, options);

    const initObserver = () => {

      observedElements.forEach((pattern) => {
        observer.observe(pattern);
      });
    }

    const scrollIntoView = require('scroll-into-view');
    const intoView = (el) => {
      // options: https://www.npmjs.com/package/scroll-into-view
      scrollIntoView(el, {
        cancellable: false,
        time: 250,
        align: {
          top: 0,
          topOffset: 64,
        },
      });
    }

    const hashChangeHandle = (event) => {
      event.preventDefault(); /* we need a safari work around for smooth scroll*/
      if (window.location.hash) {
        let hash = window.location.hash;
        let id = hash.substring(1);
        let linkTo = document.querySelector('[data-hash="' + id + '"]' );
        intoView(linkTo);
      }
    }

    const bindEvents = () => {
      window.addEventListener('hashchange', hashChangeHandle, true);
      window.addEventListener('load', hashChangeHandle, true);
    };

    const init = () => {
      initObserver();
      bindEvents()
    };

    init();

  }
})();

export default observeDecisions;
