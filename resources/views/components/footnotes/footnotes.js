const footnotes = (function footnotes(obj) {
  const footnotesSelector = 'ol.is-style--footnote';
  const footnotesListElements = 'li';
  const footnotesLinkSelector = '[data-role="jump"]'
  const jumps = document.querySelectorAll(footnotesLinkSelector);
  const scrollIntoView = require('scroll-into-view');

  const initFootnotes = (element) => {
    if (element) {
      console.info('footnote list found: ', element);

      const randomID = Math.random().toString(36).substring(2);

      element.setAttribute('id', randomID);
      let orderedListElements = element.querySelectorAll(footnotesListElements);
      let section = element.parentElement;
      let references = section.querySelectorAll(footnotesLinkSelector);

      /**
       * Prepare <ol> <li> id's
       */
      Array.prototype.forEach.call(orderedListElements, function (listElement, index) {
        const listID = randomID + '--' + (index + 1);
        console.info('listID: ', listID);
        listElement.setAttribute('id', listID);
        listElement.setAttribute('data-role', 'back');
        listElement.classList.add('cursor-pointer', 'footnote');

        listElement.addEventListener('click', function (event) {
          let id = event.target.id;
          let element = document.querySelector('[data-id="' + id + '"]');
          intoView(element);
        })
      })

      /**
       * Prepare jump anchors
       */
      Array.prototype.forEach.call(references, function (reference) {
        let referenceID = reference.getAttribute('data-id').split('#')[1];
        if (referenceID) {
          let jumpID = randomID + '--' + referenceID;
          console.info('jumpID: ', jumpID);
          reference.setAttribute('data-id', jumpID);
        }
      })
    }
  }

  /**
   * add eventListeners
   */
  Array.prototype.forEach.call(jumps, (jump) => {
    jump.addEventListener('click', function (event) {
      let id = event.target.dataset.id;
      let element = document.getElementById(id);
      intoView(element);
    });
  })

  /**
   * scroll into view & handle active state
   */
  const intoView = (el) => {
    scrollIntoView(el, {
      cancellable: false,
      time: 1250,
      align: {
        top: 0,
        topOffset: 64,
      },
    }, () => {
      let orderedListElements = document.querySelectorAll(footnotesListElements);
      let footnotes = document.querySelectorAll(footnotesLinkSelector);

      Array.prototype.forEach.call(footnotes, function (footnote) {
        footnote.classList.remove('is-active');
      })

      Array.prototype.forEach.call(orderedListElements, function (listElement) {
        listElement.classList.remove('is-active');
      })
      el.classList.toggle('is-active')
    });
  }

  /**
   * debounce scroll
   */
  const throttle = (fn, wait) => {
    let time = Date.now();
    return function () {
      if ((time + wait - Date.now()) < 0) {
        fn();
        time = Date.now();
      }
    }
  }

  /**
   * remove active state when scrolling
   */
  const scrollHandle = () => {
    let orderedListElements = document.querySelectorAll(footnotesListElements);
    let footnotes = document.querySelectorAll(footnotesLinkSelector);

    Array.prototype.forEach.call(footnotes, function (footnote) {
      footnote.classList.remove('is-active');
    })

    Array.prototype.forEach.call(orderedListElements, function (listElement) {
      listElement.classList.remove('is-active');
    })
  }

  const bindEvents = () => {
    window.addEventListener('scroll', throttle(scrollHandle, 4000), false);
  }

  const init = () => {
    bindEvents();
  };

  init();

  if (obj) {
    //console.log(obj.entry.target);
    let footnote = obj.entry.target.querySelector(footnotesSelector);
    console.info(footnote + ' fetched footnote references in ' + obj.entry + ' found');
    initFootnotes(obj.entry.target);

  } else {
    /* not called via fetch/axios */
    let footnote = document.querySelector(footnotesSelector);
    console.info(footnote + ' global footnote reference found');
    initFootnotes(footnote);
  }

  return footnotes;
})();

export default footnotes;

