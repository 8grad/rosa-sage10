<div class="grid grid-cols-12 fixed left-0 bottom-0 w-full h-0 mb-16 bottom-0 h-0 z-2">
  <div class="col-span-12 md:col-span-8 md:col-start-5 h-0">
    <div class="scroll-to-top w-full flex items-center justify-center md:hidden h-0"
         data-component="scroll-to-top">
      <a title="back" data-role="move-back"
         class="flex items-center justify-center w-10 h-10 bg-black rounded-full shadow-default mr-2 text-gray-300 hover:text-white"
         href="{{ site_url() }}/de/positions/">@fas('compress-alt', 'pointer-events-none')</a>
      <a title="top" data-role="move-top"
         class="flex items-center justify-center w-10 h-10 bg-black rounded-full shadow-default text-gray-300 hover:text-white"
         href="{{ site_url() }}/de/positions/#top">@fas('arrow-up' , 'pointer-events-none')</a>
    </div>
  </div>
</div>
