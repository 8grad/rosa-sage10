const scrollToTop = (function scrollToTop() {
  const scrollToTopSelector = '[data-component="scroll-to-top"]';
  const topSelector = '[data-role="move-top"]';
  const backSelector = '[data-role="move-back"]';
  const toggleSelector = '[data-role="radio"]';
  const isVisibleClass= 'is-visible';
  const isActiveClass= 'is-active';

  const radios = document.querySelectorAll(toggleSelector);
  const element = document.querySelector(scrollToTopSelector);
  const top = document.querySelector(topSelector);
  const back = document.querySelector(backSelector);

  const topDistance = 90;
  const scrollIntoView = require('scroll-into-view');

  if (element) {

    const scrollHandle = () => {
      let y = window.scrollY;

      if (y > topDistance) {
        element.classList.add(isVisibleClass);
      } else {
        element.classList.remove(isVisibleClass);
      }
    };


    const intoView = (el) => {
      // options: https://www.npmjs.com/package/scroll-into-view
      scrollIntoView(el, {
        cancellable: false,
        time: 1250,
        align: {
          top: 0,
          topOffset: 0,
        },
      });
    }

    const closeCols = () => {
      Array.prototype.forEach.call(radios, function (radio) {
        radio.checked = false;
        radio.closest('.custom-post-cols__section').classList.remove(isActiveClass);
      });
    }


    const clickHandle = (event) => {
      event.preventDefault(); /* we need a safari work around for smooth scroll*/
      if(event.target.hash) {
        let hash = event.target.hash;
        let id = hash.substring(1);
        let linkTo = document.getElementById(id);
        intoView(linkTo);
      } else {
        closeCols();
      }
    };

    /**
     * debounce scroll
     */
    const throttle = (fn, wait) => {
      var time = Date.now();
      return function () {
        if ((time + wait - Date.now()) < 0) {
          fn();
          time = Date.now();
        }
      }
    }

    const bindEvents = () => {
      window.addEventListener('scroll', throttle(scrollHandle, 250), false);
      top.addEventListener('click', clickHandle, false);
      back.addEventListener('click', clickHandle, false);
    };

    const init = () => {
      bindEvents();
    };

    init();
  }
  return scrollToTop;
})
();

export default scrollToTop;
