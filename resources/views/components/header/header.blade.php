<header class="banner hidden">
  <div class="container mx-auto">
    <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
  </div>
</header>
