@php
    $id = get_the_ID();
    $bgColor = get_field('color_selection', $id);
    $queried_post_type = get_query_var('post_type');

@endphp
<main class="main col-span-12 col-start-4 xl:col-start-3 flex-0 md:w-3/4 lg:w-4/5 xl:w-5/6">
  <div class="grid grid-cols-12 content-start min-h-full min-h-screen <?= $queried_post_type != 'decisions' ? $bgColor : 'bg-transparent' ?> ">
    @yield('content')
  </div>
</main>
