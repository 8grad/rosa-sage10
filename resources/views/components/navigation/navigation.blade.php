<?php

use function Roots\asset;
$homeLink = "Rosa Luxemburg — Home";
?>

<nav data-role="navigation" class="navigation fixed md:relative shadow-default w-full bg-white z-10 md:bg-white md:shadow-none flex-1 md:w-1/4 low:w-full lg:w-1/5 xl:w-1/6 md:h-full">
  <input class="navigation__input absolute hidden" type="checkbox" id="menu__trigger">

  <nav class="navigation__mobile flex items-center md:hidden p-3 text-lg leading-none h-12">
    <div class="navigation__mobile-logo flex-1 flex items-center">
      <a class="flex items-center justify-center" href="{{ get_bloginfo('url', 'display') }}">
        <img class="h-6 max-w-none" src="<?= asset( 'images/RSLXMRG.svg' ) ?>" alt="RSLXMRG Startpage" />
      </a>
    </div>

    <div class="flex-1 text-lg text-right">
      <label class="navigation__label -mb-1 text-lg md:hover:text-gray-300 cursor-pointer text-lg flex items-start justify-end -mt-1" for="menu__trigger">
        @fas('bars')
      </label>
    </div>
  </nav>

  <aside class="navigation__sidebar flex fixed bg-white md:w-1/4 lg:w-1/5 xl:w-1/6 top-0 h-full md:h-screen flex-col justify-between p-3 overflow-hidden z-10 md:z-auto">
    <a class="no-underline" href="{{ get_bloginfo('url', 'display') }}" title="{{$homeLink}}">
      <img class="max-w-xs" src="<?= asset( 'images/RSLXMRG.svg' ) ?>" alt="{{$homeLink}}" aria-label="{{$homeLink}}" />
    </a>

    @include('components.navigation.sidebar')
    @include('components.notification.notification')
    @include('components.footer.footer')

    <!-- social media links -->
    <ul class="flex text-sm xl:text-lg text-white mb-2 mt-2 md:mb-2 md:mt-6">
      <!-- https://www.instagram.com/rosaluxstiftung/ -->
      <!-- https://www.facebook.com/search/posts/?q=rslxmbrg&filters=eyJycF9hdXRob3IiOiJ7XCJuYW1lXCI6XCJhdXRob3JcIixcImFyZ3NcIjpcIjEzNDQ4Mzk3OTExOVwifSJ9&epa=FILTERS-->
      <!-- https://www.youtube.com/playlist?list=PL4D0A83B73DBA7B46 -->
      <!-- https://twitter.com/search?q=(%23rslxmbrg)%20(from%3Arosaluxstiftung)&src=typed_query&f=live -->
      <li class="mr-3"><a class="w-8 h-8 xl:w-10 xl:h-10 bg-black shadow-default rounded-full block flex items-center justify-center" target="_blank" rel="noopener" title="Instagram" href="<?=__('https://www.instagram.com/rosaluxstiftung/', 'sage')?> "><span class="hidden">Instagram</span>@fab('instagram')</a></li>
      <li class="mr-3"><a class="w-8 h-8 xl:w-10 xl:h-10 bg-black shadow-default rounded-full block flex items-center justify-center" target="_blank" rel="noopener" title="Facebook" href="<?= __('https://www.facebook.com/rosaluxglobal', 'sage') ?>"><span class="hidden">Facebook</span>@fab('facebook-f')</a></li>
      <li class="mr-3"><a class="w-8 h-8 xl:w-10 xl:h-10 bg-black shadow-default rounded-full block flex items-center justify-center" target="_blank" rel="noopener" title="Youtube" href="<?= __('https://www.youtube.com/playlist?list=PL4D0A83B73DBA7B46', 'sage') ?>"><span class="hidden">Youtube</span>@fab('youtube')</a></li>
      <li class="mr-0"><a class="w-8 h-8 xl:w-10 xl:h-10 bg-black shadow-default rounded-full block flex items-center justify-center" target="_blank" rel="noopener" title="Twitter" href="<?= __('https://twitter.com/rosalux_global', 'sage') ?>"><span class="hidden">Twitter</span>@fab('twitter')</a></li>
    </ul>

  </aside>
</nav>
