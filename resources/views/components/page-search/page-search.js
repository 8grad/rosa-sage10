const pageSearch = (function pageSearch() {
  const pageSearchSelector = '[data-filter]';
  const inputFilter = document.querySelector(pageSearchSelector);

  if (inputFilter) {
    const searchHandle = (event) => {
      let inputValue = event.target.value, i;
      let filterList = document.getElementById(inputFilter.dataset.filter);
      let filterItem = filterList.querySelectorAll("p");

      for (i = 0; i < filterItem.length; i++) {
        let filtered = filterItem[i];
        let phrase = filtered.innerHTML;

        if (phrase.search(new RegExp(inputValue, "i")) < 0) {
          filtered.classList.add('hidden');
        } else {
          filtered.classList.remove('hidden');
        }
      }
    }

    const bindEvents = () => {
      inputFilter.addEventListener('keyup', searchHandle, false);
    };

    const init = () => {
      bindEvents();
    };

    init();
  }
  return pageSearch;
})
();

export default pageSearch;
