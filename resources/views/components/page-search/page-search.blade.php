<div class="page-search mb-8 flex items-end justify-end" data-component="page-search">
  <label for="page-search"></label>
  <input id="page-search" class="p-2 shadow-default font-sans" type="search" data-filter="search" placeholder="<?= __('Search', 'sage')?>">
</div>
