import inlineImage from '../inline-image/inline-image';
import figure from '../figure-with-popover/figure-with-popover';
import footnotes from "../footnotes/footnotes";

const observeCols = (function () {
  const axios = require('axios');
  const scrollIntoView = require('scroll-into-view');

  const componentSelector = '[data-component="custom-post-cols"]';
  const navSelector = '[data-role="navigation"]';
  const innerWrapperSelector = '[data-role="custom-post-col"]';
  const sectionsSelector = '[data-role="section"]';
  const toggleSelector = '[data-role="radio"]';
  const observeSelector = '[data-role="observe-intersection"]';
  const postTypeSelector = '[data-post-type]';
  const jsonEndpoint = '/wp-json/wp/v2/';
  const activeClassName = "is-active"

  const element = document.querySelector(componentSelector);
  const navHeight = document.querySelector(navSelector).getBoundingClientRect().height;

  const options = options || {
    root: element,
    rootMargin: '0px',
    threshold: 1,
  };

  if (element) {
    let sections = element.querySelectorAll(sectionsSelector);
    let observables = element.querySelectorAll(observeSelector);
    let radios = element.querySelectorAll(toggleSelector);
    let postType = element.querySelectorAll(postTypeSelector);

    let axiosRequest = (id, entry) => {
      let siteUrl = element.dataset.url;
      let pageFragment = jsonEndpoint + postType[0].dataset.postType + '/';
      let jsonUrl = siteUrl + pageFragment + id;

      axios.get(jsonUrl)
        .then((response) => {

          // Pluck the .data array from the response.
          const post = response.data;
          const container = document.getElementById('c-custom-post-cols__axios--' + post.id);

          container.innerHTML = post.content.rendered;

        }).then(function () {

        /**
         * re-init plugins
         */
        inlineImage({entry});
        figure({entry});
        footnotes({entry});

      }).catch(function (error) {
        // handle error
        console.error(error);
      })
        .then(function () {
          //  always executed
        });

    };

    let callback = (entries) => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          // get the id
          let id = entry.target.closest(innerWrapperSelector).dataset.id;
          console.info('intersecting with ' + entry.target.baseURI);
          observer.unobserve(entry.target);
          axiosRequest(id, entry);
        }
      });
    };

    let observer = new IntersectionObserver(callback, options);

    observables.forEach((pattern) => {
      observer.observe(pattern);
    });

    const locationHash = () => {
      let locationHash = location.hash.substr(1);
      let params = locationHash.split('/');

      if (params[0]) {
        let activateCol = radios[(params[0] - 1)];
        if (activateCol) {
          activateCol.checked = true;
          setActiveClass(activateCol);
        }
      }
    }

    const loading = () => {
      sections.forEach((section) => {
        section.classList.add('is-loaded');
      });
    }

    const intoView = (el) => {

      window.setTimeout(function () {
        scrollIntoView(el, {
          align: {
            topOffset: window.innerHeight / 5 + navHeight,
          },
        });
      }, 500)
    }

    const setActiveClass = (el) => {
      let actives = element.querySelectorAll('.' + activeClassName);
      let target = el.parentElement;

      actives.forEach((active) => {
        active.classList.remove(activeClassName);
      });

      if (el.checked) {
        target.classList.add(activeClassName);
        intoView(target);
      } else {
        target.classList.remove(activeClassName);
      }
    }

    const radioChangeListener = () => {
      Array.prototype.forEach.call(radios, function (radio, index) {
        radio.addEventListener('change', function () {

          setActiveClass(this);
          window.history.pushState('page ' + index + 1, document.title +
            ' 0' + (index + 1) + '/✭', '#' + (index + 1) + '/'
          )
          ;
        });
      });
    }

    const hashChangeListener = () => {
      document.addEventListener('readystatechange', locationHash, true);
    }

    const windowListener = () => {
      document.fonts.ready.then(loading);
    }

    const init = () => {
      radioChangeListener();
      hashChangeListener();
      windowListener();
    };


    init();
  }
})();

export default observeCols;
