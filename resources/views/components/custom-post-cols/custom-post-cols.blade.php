<div class="custom-post-cols col-span-12 md:flex flex-nowrap md:flex-row md:flex-nowrap h-full overflow-hidden"
     data-component="custom-post-cols" data-url="{{ site_url() }}">

  @posts($positions)

  <div data-id="{{get_the_ID()}}"
       data-role="custom-post-col"
       class="custom-post-cols__section relative shadow-col">

    <input class="custom-post-cols__input top-0 absolute left-0 hidden"
           name="custom-posts-cols"
           type="radio"
           data-role="radio"
           id="section_{{get_the_ID()}}"
    />
    <label accesskey="{{ get_post_field( 'menu_order', get_the_ID() )  }}"
           tabindex="0"
           title="@title"
           class="custom-post-cols__label absolute"
           for="section_{{get_the_ID()}}"
           data-post-type="{{get_post_type()}}"></label>

      @php($order = get_post_field( 'menu_order', get_the_ID() ))
    <a href="{{get_the_permalink()}}#{{$order}}" aria-labelledby="Position {{$order}}" class="sr-only">Position {{$order}}</a>

    <section class="h-full" data-role="section">

      <article class="custom-post-cols__wrapper h-full" style="background-color: @field('color') ">
        <div class="custom-post-cols__container w-full min-w-full md:min-w-cols w-full h-full content md:mb-16">

          <!-- grid -->
          <div class="grid grid-cols-12 px-3 pb-32">
            <div class="custom-post-cols__counter col-span-6 pt-2 pb-2 md:pb-20"></div>

            <div class="custom-post-cols__content grid grid-cols-12 col-span-12">
              <h2 class="h1 mb-2 c-custom-post-cols__headline col-span-12 md:col-span-11">@title</h2>
              <h3 class="h6 mb-10 md:mb-20 col-span-12 md:col-span-11">@field('subheadline') <?php edit_post_link( '<i class="fas fa-user-edit"></i>', '<span title="edit article">', '</span>' ); ?></h3>

              <div class="custom-post-cols__entry entry-content my-2 w-full col-span-12 md:col-span-9 md:col-start-4">
                <section class="border border-transparent" data-role="observe-intersection">
                  <div id="c-custom-post-cols__axios--{{get_the_ID()}}"></div>

                  <div class="sr-only" aria-labelledby="{{ strip_tags(get_the_content()) }}">
                    {!! get_the_content() !!}
                  </div>
                </section>
              </div>
            </div>
          </div>

        </div>
      </article>

    </section>

  </div>

  @endposts
</div>

@include('components.scroll-to-top.scroll-to-top')
