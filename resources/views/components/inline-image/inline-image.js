import tippy from 'tippy.js';
import 'tippy.js/themes/light.css';

const inlineImage = (function inlineImage(obj) {
  const axios = require('axios');
  const bodySelector = 'body';
  const jsonEndpoint = '/wp-json/wp/v2/';
  const componentSelector = 'p > img + strong';
  const tippyImageClassName = 'tippy__image';
  const tooltipInnerClassName = 'tippy__inner';
  const tooltipInnerContentClassName = 'tippy__link';
  const tooltipInnerInfoClassName = 'tippy__link';
  const tooltipInnerButtonClassName = 'inline-image__close-button';

  const body = document.querySelector(bodySelector);

  /**
   * setup inlineImage component
   */
  const createPopOverFromMarkup = (elements) => {
    Array.prototype.forEach.call(elements, function (element) {
      let shortcode = element.innerHTML.replace(/&nbsp;/g, '').replace(/(<([^>]+)>)/gi, "").split('|');
      let moreUrl = shortcode[0];
      let moreText = shortcode[1];

      let tippyTrigger = element.previousElementSibling
      tippyTrigger.classList.add('inline-image__image');
      tippyTrigger.dataset.infotext = moreText;
      tippyTrigger.dataset.infolink = moreUrl;

      element.classList.add('hidden');
      element.dataset.role = "component-info";
    })
  }

  /**
   * setup tippy instances
   */
  const initTippy = (elements) => {
    createPopOverFromMarkup(elements);

    let tooltip = document.createElement('div');
    let tooltipContent = document.createTextNode("loading…");
    tooltip.appendChild(tooltipContent);

    tippy('.inline-image__image', {
      appendTo: document.body,
      theme: 'light',
      hideOnClick: true,
      trigger: 'click',
      allowHTML: true,
      content: tooltip,
      placement: 'bottom',
      popperOptions: {
        strategy: 'fixed',
        modifiers: [
          {
            name: 'flip',
            options: {
              fallbackPlacements: ['left', 'right'],
            },
          },
          {
            name: 'preventOverflow',
            options: {
              altAxis: true,
              tether: false,
            },
          },
        ],
      },
      zIndex: 10,
      interactive: true,
      offset: [0, -60],
      arrow: false,
      animation: 'scale-extreme',
      maxWidth: false,

      onShow(instance) {
        let imageClass = instance.reference.classList[0];
        let imageID = imageClass.split('-').pop();
        let siteUrl = body.dataset.url;
        let more = body.dataset.more;
        let jsonUrl = siteUrl + jsonEndpoint + 'media/' + imageID;

        axios.get(jsonUrl)
          // If request is successful, return the response.
          .then((response) => {

            // Pluck the .data array from the response.
            let data = response.data;

            // Create an image
            let image = new Image();
            image.width = 500;
            image.height = 200;
            image.style.display = 'block';
            image.src = data.source_url;
            image.classList.add(tippyImageClassName);

            // Update the tippy content with the image
            let tooltipInner = document.createElement('div');
            let tooltipInnerContent = document.createElement('a');
            let tooltipInnerInfo = document.createElement('div');
            let closeButton = document.createElement('button');

            closeButton.classList.add(tooltipInnerButtonClassName);
            closeButton.innerHTML = "<i class='fas fa-times pointer-events-none'>";

            tooltipInnerContent.innerHTML = more;
            tooltipInnerContent.setAttribute('href', instance.reference.dataset.infolink);
            tooltipInnerContent.classList.add(tooltipInnerContentClassName)
            tooltipInnerContent.classList.add('underline');

            tooltipInnerInfo.innerHTML = instance.reference.dataset.infotext;
            tooltipInnerInfo.classList.add(tooltipInnerInfoClassName);

            tooltipInner.classList.add(tooltipInnerClassName);
            tooltipInner.appendChild(image);
            tooltipInner.appendChild(tooltipInnerInfo);

            let linkTo = tooltipInnerContent.getAttribute('href');
            var linkToTrimmed = linkTo.trim();

            if ( linkToTrimmed !== '#') {
              tooltipInner.appendChild(tooltipInnerContent);
            }

            tooltipInner.appendChild(closeButton);
            handleClose(instance, closeButton);

            instance.setContent(tooltipInner);

          }).catch(function (error) {
          // handle error
          console.error(error);
          instance.setContent(`Request failed. ${error}`);
        })
          .then(function () {
            // always executed
          });
      },
    });
  }

  /**
   * handle tippy init
   */
  const initInlineImages = (elements) => {
    if (elements.length >= 1) {
      initTippy(elements);
    }
  }

  /**
   * handle tippy close button
   */
  const handleClose = (instance, el) => {
    el.addEventListener('click', function () {
      instance.hide();
    })

    window.addEventListener('keydown', function (e) {
      if ((e.key == 'Escape' || e.key == 'Esc' || e.keyCode == 27) && (e.target.nodeName == 'BODY')) {
        e.preventDefault();
        instance.hide();
        return false;
      }
    }, true)
  }

  if (obj) {
    let inlineImages = obj.entry.target.querySelectorAll(componentSelector);
    initInlineImages(inlineImages);
  } else {
    /* not called via fetch/axios */
    let inlineImages = document.querySelectorAll(componentSelector);
    console.info(inlineImages.length + ' inlineImages instanciated');
    initInlineImages(inlineImages);
  }

  return inlineImage; /* make sure, this IIFE function is callable */
})();

export default inlineImage;
