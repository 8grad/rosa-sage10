@extends('layouts.app')
@section('content')
  @include('partials.page-header')

  <?php
  $excludeID = get_category_by_slug('blog');
  $args = array(
    'parent'     => 0, // To get the top level categories only, set parent value to zero.
    'exclude'    => $excludeID->term_id,
    'hide_empty' => false
  );

  $categories = get_categories($args);
  $currentCategoryID = $categories[0]->term_taxonomy_id;
  App\materialCategories($currentCategoryID);
  ?>

  <div class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4">
    @if (! have_posts())
      <x-alert type="warning">
        {!! __('Sorry, no results were found.', 'sage') !!}
      </x-alert>

      {!! get_search_form(false) !!}
    @endif

    <?php
    $slug = 'citations';
    $citationsObject = get_category_by_slug($slug);
    $citationsId = $citationsObject->term_id;

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $material = new WP_Query([
        'post_type'        => 'material',
        'orderby'          => 'title',
        'order'            => 'ASC',
        'posts_per_page'   => -1,
        'paged'            => $paged,
        'category__not_in' => $citationsId,
      ]
    );
    ?>

    @posts($material)
    @includeFirst(['partials.content-' . get_post_type(), 'partials.content'])
    @endposts

    {!! get_the_posts_navigation() !!}
    @endsection
  </div>

  @include('components.scroll-to-top.scroll-to-top')


@section('sidebar')
  @include('partials.sidebar')
@endsection
