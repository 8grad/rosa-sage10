@include('archive-positions')
<?php
    $permalinkStructure   = get_option( 'permalink_structure' );
    $permalinkArray       = explode( '/', $permalinkStructure );
    $permalinkArrayLength = count( $permalinkArray );

    if ( $permalinkArrayLength >= 3 ) {
        $postGroup = $permalinkArray[1]; // '/archive';
    } else {
        $postGroup = $permalinkArray[0];
    }

    $queried_post_type = get_query_var( 'post_type' );
    $post_id           = get_the_ID();
    $order             = get_post_field( 'menu_order', $post_id );

    if ( function_exists( 'pll_current_language' ) ) {
        $baseLangPath = '/' . pll_current_language( 'slug' );
    } else {
        $baseLangPath = '';
    }
    $newUrl = get_site_url() . $baseLangPath . '/' . $queried_post_type . '#' . $order . '/';

?>

<meta http-equiv="refresh" content="5; URL=<?= $newUrl ?>">
