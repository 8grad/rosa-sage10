@php
  $slug = 'decisions';
  $header = new WP_Query( [
    'post_type'           => 'decisions',
    'orderby'             => 'menu_order',
    'order'               => 'ASC',
    'posts_per_page'      => 1,
    ]
  );

  $positions = new WP_Query( [
    'post_type'           => 'decisions',
    'orderby'             => 'menu_order',
    'order'               => 'ASC',
    'posts_per_page'      => -1,
    ]
  );
@endphp

@extends('layouts.app')

@section('content')

    @posts($header)
    @includeFirst(['partials.content-' . get_post_type(). '-header', ''])
    @endposts

    <div class="decisions col-span-12" data-component="decisions" data-post-type="decisions">
        @if (! have_posts())
            <x-alert type="warning">
                {!! __('Sorry, no results were found.', 'sage') !!}
            </x-alert>

            {!! get_search_form(false) !!}
        @endif

        @php
            $count = 0;
        @endphp

        @posts($positions)
        @php
            $count = $count + 1;
        @endphp
        @includeFirst(['partials.content-' . get_post_type(), 'partials.content'])
        @endposts
    </div>

@endsection

@section('sidebar')
    @include('partials.sidebar')
@endsection

@include('components.scroll-navigation.scroll-navigation')
