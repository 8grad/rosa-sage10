@include('components.header.header')

<div id="top" class="wrap" role="document" data-role="debug">
  <div class="content flex">

    @include('components.navigation.navigation')
    @include('components.main.main')

  </div>
</div>

{{--@include('components.debug.debug')--}}
