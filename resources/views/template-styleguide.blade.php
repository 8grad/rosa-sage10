{{--
  Template Name: Styleguide Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-header')

  <div class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4">
    {{ App\styleguide()  }}

    <h3 class="mb-4">Components:</h3>
    @include('partials.content-page')
    @endwhile
  </div>
@endsection


