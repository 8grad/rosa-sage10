<?php
/**
 *
 * Single entry  custom post redirect;
 *
 */

$permalinkStructure = get_option('permalink_structure');
$permalinkArray = explode('/', $permalinkStructure);
$permalinkArrayLength = count($permalinkArray);

if ($permalinkArrayLength >= 3) {
  $postGroup = $permalinkArray[1]; // '/archive';
} else {
  $postGroup = $permalinkArray[0];
}

$queried_post_type = get_query_var('post_type');
$post_id = get_the_ID();
$order = get_post_field('menu_order', $post_id);

if (function_exists('pll_current_language')) {
  $baseLangPath = '/' . pll_current_language('slug');
} else {
  $baseLangPath = '';
}

//http://rosa-prototype.local/de/archive/positions#1/
$newUrl = get_site_url() . $baseLangPath . '/' . $queried_post_type . '/#' . $order;

if ( is_single() && 'decisions' == $queried_post_type ) {
?>
<meta http-equiv="refresh" content="5; URL=<?= $newUrl ?>"><?php
wp_redirect($newUrl, 303);
exit;
}

/*var_dump('trying to redirect');
var_dump($queried_post_type);
var_dump($post_id);
var_dump($order);
var_dump('New Url: ', $newUrl);
var_dump('Lang: ', $baseLangPath);
var_dump('structure: ', $permalinkStructure);
var_dump('structured: ', $permalinkArray);
var_dump('size: ', $permalinkArrayLength);*/
