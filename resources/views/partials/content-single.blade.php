<article @php(post_class('px-3 md:px-0 col-span-12 grid grid-cols-12'))>
  <header class="px-3 md:px-0  col-span-12 md:col-span-11 mt-20 md:mt-30">
    <h1 class="">
      {!! $title !!}
    </h1>
  </header>

  <div class="px-3 md:px-0 pb-20 col-span-12 md:col-span-8 md:col-start-4">
    <?php
    if (!empty(get_the_tags())) {
      the_tags(__('Tagged with', 'sage') . ': <span class="p-1 bg-black p-1 font-sans px-3 rounded-2xl text-white no-underline"> ', '</span> <span class="p-1 bg-black text-white">', '</span><br />');
    }
    ?>
    @php(the_content())
  </div>

  <footer class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4">
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>

</article>

@include('components.scroll-to-top.scroll-to-top')
