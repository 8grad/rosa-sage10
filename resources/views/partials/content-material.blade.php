<article @php(post_class('mb-20'))>
  <header>
    <span class="hidden mr-2">#{{get_the_ID()}}</span>
    <?php
    global $post;
    $postcats = get_the_category($post->ID);
    $postCatArray = [];
    if (!empty($postcats)) {
      foreach ($postcats as $postcat) {
        $postCatArray[] = "<span class='text-xs md:text-sm italic mb-1'>" . esc_html($postcat->name) . "</span>";
      }
      echo implode(' / ', $postCatArray);
    }
    ?>
    <?php edit_post_link('<i class="fas fa-user-edit"></i>', '<span title="edit article">', '</span>'); ?>
    <a class="underline" href="{{ the_permalink() }}">
      <h2 class="h2">
        {!! $title !!}
      </h2>
    </a>

    <p>
      <?php
      if (!empty(get_the_tags())) {
        the_tags(__('Tagged with', 'sage') . ': <span class="shadow-default p-1 bg-black p-1 font-sans px-3 rounded-2xl text-white no-underline"> ', '</span> <span class="p-1 bg-black p-1 font-sans px-3 rounded-2xl text-white no-underline">', '</span><br />');
      }
      ?>
    </p>
  </header>

  <div class="entry-content">

    <span class="inline italic">@field('subheadline')</span>
    @php(the_excerpt())

    <p>
      <a class="button" href="<?= get_permalink() ?>"><span> <?=  __('Continued', 'sage') ?></span><i
          class="ml-4 inline-block fas fa-arrow-right"></i>
      </a>
    </p>

  </div>

  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
</article>
