<header class="col-span-12 grid grid-cols-12 mt-10 md:p-0 sm:mt-10 md:mt-30 xl:mt-30 mb-30">
  <h1 class="col-span-12 md:col-span-11 pb-2 m-0 z-0 " style="z-index: 1">@field('intro-headline')</h1>
  <div class="h6 col-span-12 md:col-span-3 mb-10">@field('intro-subline')</div>

  <img class="wp-block-image w-full col-span-10 col-start-3 col-end-12 md:col-span-8 mb-4 max-w-none"
       src="@field('intro-image', 'url')" alt=""/>

  <p class="col-span-10 col-start-3 col-end-12 md:col-span-8 md:col-start-4 font-sans text-xs leading-relaxed mb-8">
    @field('intro-image-description')
  </p>

  <p class="col-span-12 md:col-start-4 md:col-end-12 is-style--bodytext-comment">
    @field('intro-text')
  </p>
</header>
