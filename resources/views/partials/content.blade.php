<article @php(post_class('border border-black border-t-0 border-r-0 border-l-0 mb-12'))>
  <header>
    <h2 class="entry-title">
      <a href="{{ get_permalink() }}">
        {!! $title !!}
      </a>
    </h2>

    @include('partials/entry-meta')
  </header>

  <div class="entry-summary">
    @php(the_content())
  </div>
</article>
