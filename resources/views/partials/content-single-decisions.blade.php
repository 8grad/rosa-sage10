<article @php(post_class('col-span-12 mt-20 md:mt-30'))>
  <header>
    <h1 class="entry-title">
      {!! $title !!}
    </h1>
  </header>

  <div class="entry-content">
    @php(the_content())

  </div>

  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>

</article>
