<?php
$excludeID = get_category_by_slug('blog');
if (is_home()) {
  $args = array(
    'parent'     => 0, // To get the top level categories only, set parent value to zero.
    'childless'  => true,
    'hide_empty' => false
  );
} else {
  $args = array(
    'parent'     => 0, // To get the top level categories only, set parent value to zero.
    'exclude'    => $excludeID->term_id,
    'hide_empty' => false
  );
}
$categories = get_categories($args);
$currentCategoryID = $categories[0]->term_taxonomy_id;
$currentCategoryName = $categories[0]->name;
$catDescription = category_description($currentCategoryID);

$recentCategoryQuery = get_category(get_query_var('cat'));
$recentCategoryID = $recentCategoryQuery->term_id;

if (!isset($recentCategoryID)) {
  $description = category_description($currentCategoryID);
  if(is_archive()) {
    $title = $currentCategoryName;
  }
} else {
  $description = "<p>" . $recentCategoryQuery->description . "</p>";
}
?>

<div class="px-3 md:px-3 mt-20 md:mt-30 col-span-12 md:col-span-11">
  <h1 class="m-0 pb-2">
    {!! $title !!}
  </h1>
  <div class="h6 mb-10 md:mb-20 col-span-12 md:col-span-11">
    {{--        <pre class="text-xxs">--}}
    {{--          @php(print_r())--}}
    {{--        </pre>--}}
    @if(is_archive())
      {!! $description !!}
    @elseif(is_home())
      {!! $description !!}
    @else
      @field('subheadline')
    @endif

    <?php edit_post_link('<i class="fas fa-user-edit"></i>', '<span title="edit article">', '</span>'); ?></div>
</div>
