<time class="italic mb-1 block" datetime="{{ get_post_time('G', true) }}">
  {{ get_the_date('d.n.Y') }}
</time>
