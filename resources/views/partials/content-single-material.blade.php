<?php
global $post;
$postcats = get_the_category($post->ID);

// http://localhost:3000/category/material/articles/
// var_dump($postcats[0]->slug);

$referer = $_SERVER["HTTP_REFERER"];
$refererArray = explode('/', $referer);

// back link
if ($refererArray[3] == 'material') {
  $backlink = site_url() . "/material/";
} else {

  if (function_exists('pll_current_language')) {
    $lang = pll_current_language('slug');
    $backlink = site_url() . "/" . $lang . "/category/material/" . $postcats[0]->slug;
  } else {
    $backlink = site_url() . "/category/material/" . $postcats[0]->slug;
  }

}
?>

<article @php(post_class('px-3 pb-20 col-span-12 grid grid-cols-12'))>
  <header class="md:px-0 col-span-12 md:col-span-11 mt-10 md:mt-30">
    <a class="button mb-8 p-2 px-4" href="<?= $backlink ?>"><i class="fas fa-arrow-left"></i></a>

    <div>
      <?php
      $postCatArray = [];
      if (!empty($postcats)) {
        foreach ($postcats as $postcat) {
          $postCatArray[] = "<span class='text-xs md:text-sm italic mb-1'>" . esc_html($postcat->name) . "</span>";
        }
        echo implode(' / ', $postCatArray);

      }
      ?>
    </div>
    <h1>
      <span class="hidden mr-2">#{{get_the_ID()}}</span> {!! $title !!}
    </h1>

    <h3 class="h6 mb-10 md:mb-20 col-span-12 md:col-span-11"> @field('subheadline') </h3>
  </header>


  <div class="md:px-0 col-span-12 md:col-span-8 md:col-start-4 mt-20">
  <?php edit_post_link('<i class="fas fa-user-edit"></i>', '<span title="edit article">', '</span>'); ?>

    <!-- soundcloud iframe section-->
    @if(get_field('id'))
      <p class="is-style--footnote">
        <em><?php the_field('footnote'); ?></em>
      </p>

      <iframe class='mb-16' width='100%' height='20' scrolling='no' frameborder='no' allow='autoplay'
              src='https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php the_field('id'); ?>&color=%23000000&inverse=false&auto_play=false&show_user=true'>
      </iframe>
    @endif

    @php(the_content())

    <?php
    if (!empty(get_the_tags())) {
      the_tags(__('Tagged with', 'sage') . ': <span class="p-1 shadow-default bg-black p-1 font-sans px-3 rounded-2xl text-white no-underline"> ', '</span> <span class="p-1 bg-black p-1 font-sans px-3 rounded-2xl text-white no-underline">', '</span><br />');
    }
    ?>

  </div>

  <footer class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4">
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>

</article>

@include('components.scroll-to-top.scroll-to-top')
