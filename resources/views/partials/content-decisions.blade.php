@php
  $count == $count + 1;
@endphp

<article data-hash="<?= $count ?>" {{--id="<?= $count ?>"--}} data-role="observe-intersection" data-id="{{ get_the_ID() }}"
         data-id="decision--{{ get_the_ID() }}" @php(post_class('grid grid-cols-12 md:pt-0 pb-16 md:pb-32 min-h-screen md:min-h-0'))>

  <header class="col-start-1 col-end-13 md:col-start-2 md:col-end-9">
    <h2 data-role="observe-intersection-for-scroll-navigation"
        data-hash="<?= $count ?>"
        class="inline-flex flex-row p-0 m-0 mb-4">

      <span class="flex-0 flex mr-2 md:mr-4 block">
        <span class="block bg-black text-yellow inline-flex items-center justify-center rounded-full
                      w-10 h-10 md:w-12 md:h-12 lg:w-16 lg:h-16 xl:w-24 xl:h-24 -mt-2">
          <span>{!! $count !!}</span>
        </span>
      </span>

      <span>
        <span class="block">{!! $title !!}</span>
        <span class="block text-base mt-2 mb-8 h6">@field('subheadline')
          <?php edit_post_link('<i class="inline h6 fas fa-user-edit"></i>', '<span title="edit article">', '</span>'); ?>
        </span>

      </span>
    </h2>

  </header>

  <div class="col-start-1 col-end-13 md:col-start-2 md:col-end-12">
     @php(the_content())
    {{-- disabled for now --}}
    {{-- we are getting the content through fetch API --}}
    {{-- <div class="opacity-0 transition-opacity" id="decision__axios--<?= get_the_ID() ?>"></div>--}}
  </div>
</article>
