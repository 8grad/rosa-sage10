<?php
  /**
   * Block Name: miniSoundCloudPlayer
   *
   * This is the template that displays the miniSoundCloudPlayer block.
   */

  $value = 'slug';
  $captions = 1;
  $videoID = get_field('youtubevideoid');

  if(function_exists('pll_current_language')) {
    $lang = pll_current_language($value);
  } else {
    $lang = "de";
  }

?>

<div class="bg-black">
  <div class="wp-block-embed wp-embed-aspect-16-9 wp-has-aspect-ratio">
    <figure class="wp-embed-aspect-16-9 wp-has-aspect-ratio wp-block-embed is-type-video is-type-translated-video">
      <div class="wp-block-embed__wrapper">
        <iframe class='mb-0'
                width='100%'
                height='auto'
                scrolling='no'
                frameborder='no'
                allow='accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                src='https://www.youtube.com/embed/<?= $videoID ?>?hl=<?= $lang ?>&cc_lang_pref=<?= $lang ?>&cc=<?= $captions ?>&cc_load_policy=1'>

        </iframe>
      </div>
    </figure>
  </div>
</div>
