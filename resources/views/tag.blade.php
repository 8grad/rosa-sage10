@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  <div class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4">

    <?php
    $tag = get_query_var('tag');

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $cat = new WP_Query( [
        'post_type'      => 'material',
        'orderby'        => 'title',
        'tag'            => $tag,
        'order'          => 'ASC',
        'posts_per_page' => - 1,
        'paged'          => $paged,
      ]
    );
    ?>

    @posts($cat)
    @includeFirst(['partials.content-' . get_post_type(), 'partials.content'])
    @endposts

    {!! get_the_posts_navigation() !!}
  </div>
@endsection

@section('sidebar')
  @include('partials.sidebar')
@endsection
