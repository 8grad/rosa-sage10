@extends('layouts.app')

@section('content')
  @php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
      $positions = new WP_Query( [
        'post_type'           => 'positions',
        'orderby'             => 'menu_order',
        'order'               => 'ASC',
        'posts_per_page'      => 5,
        'paged'               => $paged,
        ]
      );
  @endphp

  @include('components.custom-post-cols.custom-post-cols')

  @php
    if (function_exists("pagination")) {
      pagination($positions->max_num_pages);

    }
  @endphp

@endsection
