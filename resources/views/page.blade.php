@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
  @include('partials.page-header')
  <div class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4 pb-32">
      @includeFirst(['partials.content-page', 'partials.content'])
  </div>
  @endwhile
@endsection

