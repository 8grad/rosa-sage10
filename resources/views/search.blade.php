@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  <div class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4">

    @if (! have_posts())
      <x-alert type="warning">
        {!! __('Sorry, no results were found.', 'sage') !!}
      </x-alert>

      {!! get_search_form(false) !!}
    @endif

    @while(have_posts()) @php(the_post())
    @include('partials.content-search')
    @endwhile
    {!! get_the_posts_navigation() !!}
  </div>

@endsection
