module.exports = {
  future: {},
  purge: {
    enabled: ((process.env.NODE_ENV === 'production') ? true : false),
    content: [
      './index.php',
      './resources/views/*.php',
      './resources/views/**/*.php',
      './resources/views/**/**/*.php',
      './storage/framework/views/*php',
      './app/*.php',
    ],
    // These options are passed through directly to PurgeCSS
    options: {
      safelist: [
        /^col-/,
        /^gap -/,
        /^cursor-/,
        /^justify-/,
        /^relative/,
        /^max-/,
        /^text-/,
        /^xl:text-/,
        /^md:text-/,
        /^lg:text-/,
        /^shadow-/,
        /^bg-/,
        /^font-/,
        /^leading-/,
        /^tracking-/,
        /^p-/,
        /^w-/,
        /^h-/,
        /^rounded-/,
        /^border-/,
        /^italic/,
        /^mb-/,
        /^sm:mb-/,
        /^sm:-mb-/,
        /^sm:mt-/,
        /^md:mb-/,
        /^md:mt-/,
        /^lg:mb-/,
        /^lg:mt-/,
        /^xl:mb-/,
        /^xl:mt-/,
        /^mt-/,
        /^mx-/,
        /^-mt-/,
        /^-ml-/,
        /^pb-/,
        /^pt-/,
        /^md:pb-/,
        /^md:pt-/,
        /^py-/,
        /^px-/,
        /^pointer-events/,
        /^uppercase/,
        /^max-h/,

      ],
    },
    css: [], // css
    fontFace: false,
  },
  theme: {
    colors: {
      green: '#47BE61',
      blue: '#1990FD',
      rose: '#FAE4E9',
      red: '#FC5555',
      yellow: '#FDEC51',
      smoke: '#ececec',
      white: '#FFFFFF',
      transparent: 'transparent',
      black: '#000000',
      gray: {
        100: '#F3F4F6',
        200: '#E5E7EB',
        300: '#D1D5DB',
        400: '#9CA3AF',
        500: '#6B7280',
        600: '#4B5563',
        700: '#374151',
        800: '#1F2937',
        900: '#111827',
      },
    },
    extend: {
      screens: {
        //'sm': '640px',
        'md': [
          {'min': '768px'},
        ],
        'lg': '960px',
        'xl': '1600px',
        'portrait': {'raw': '(orientation: portrait)'},
        'low': {'raw': '(max-height: 1234px)'},
      },
      borderWidth: {},
      rotate: {
        '-30': '-30deg',
      },
      zIndex: {
        '0': 0,
        '2': 2,
        '10': 10,
        '20': 20,
        '30': 30,
        '40': 40,
        '50': 50,
        '75': 75,
        '100': 100,
        'auto': 'auto',
      },
      boxShadow: {
        col: '-10px 0 10px 0 rgba(0, 0, 0, 0.25)',
        default: '2px 2px 4px 0 rgba(0, 0, 0, 0.25)',
        focus: '0.0625rem 0.0625rem 0 #fdec51, -0.0625rem -0.0625rem 0 #fdec51',
      },
      margin: {
        '12': '3rem',
        '14': '3.5rem',
        '30': '7.5rem',
        '60': '15rem',
        '64': '16rem',
        '80': '20rem',
      },
      padding: {
        '2/3': '66%',
        '3/3': '100%',
      },
      spacing: {
        '144': '36rem',
      },
    },
    container: {
      padding: '0.5rem',
    },
    lineHeight: {
      'none': '1',
      'tight': '1.045',
      'snug': '1.1',
      'normal': '1.2',
      'relaxed': '1.3',
      'loose': '1.7',
      '1': '3rem',
      '2': '3rem',
      '3': '3rem',
      '4': '3rem',
      '5': '3rem',
      '6': '1.5rem',
    },
    fontSize: {
      'xxs': '.75rem',      // 12px
      'xs': '.875rem',      // 14px
      'sm': '1.0625rem',    // 17px
      'default': '1rem',    // 16px
      'base': '1.125rem',   // 18px
      'lg': '1.5rem',       // 24px
      'sxl': '1.75rem',     // 28px
      'xl': '2.25rem',      // 36px
      '2xl': '3rem',        // 48px
      '3xl': '0',           // free
      '4xl': '4rem',        // 64px
      '5xl': '4.75rem',     // 76px
      '6xl': '6rem',        // 96px
    },
    letterSpacing: {
      tightest: '-.075em',
      tighter: '-.05em',
      tight: '-.025em',
      normal: '0',
      wide: '.01em',
      wider: '.02em',
      widest: '.1em',
    },

    fontFamily: {
      el: ['\"Karloff Positive Bold\"', 'serif'],
      elRegular: ['\"Karloff Positive Regular\"', 'serif'],
      icons: ['"Font Awesome 5 Free"'],
      serif: ['nbsp', '\"Noe Text\"', '\"Karloff Positive Regular\"', 'Georgia', 'Cambria', '\"Times New Roman\"', 'Times', 'serif'],
      display: ['\"Noe Display\"', 'Georgia', 'Cambria', '\"Times New Roman\"', 'Times', 'serif'],
      sans: ['Silka', 'Arial', 'Helvetica', 'sans-serif'],
      mono: [
        'Menlo',
        'Monaco',
        'Consolas',
        '\"Liberation Mono\"',
        '\"Courier New\"',
        'monospace',
      ],
    },

    maxHeight: {
      '0': '0',
    },
    minWidth: {
      'cols': 'calc(100vw - (100vw / 2))',
    },

  },
  variants: {
    // Some useful comment
  },
  plugins: [
    require("tailwindcss-hyphens"),
  ],
};

