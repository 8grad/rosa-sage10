<?php
    add_action('acf/init', 'my_acf_init');

    function my_acf_init() {
        //check function exists
        if (function_exists('acf_register_block')) {

            //register a testimonial block
            acf_register_block(array(
                'name' => 'translatedVideoPlayer',
                'title' => __('RSLXMBRG translated Youtube Video Player'),
                'description' => __('A custom miniplayer block.'),
                'render_callback' => 'my_acf_block_render_callback',
                'category' => 'media',
                'icon' => 'admin-comments',
                'keywords' => array('audio'),
            ));
        }
    }

    function my_acf_block_render_callback($block) {
        $file = get_template_directory() . "/resources/views/blocks/translatedVideoPlayer.php";

        if (file_exists($file)) {
            require($file);
        }
    }
