<?php

// remove default block patterns
remove_theme_support( 'core-block-patterns' );


register_block_pattern(
    'Inline Image with PopOver',
    array(
        'title'       => __( 'Pattern for inline Images with popOver', 'roots' ),
        'description' => _x( 'Place me for inline images', 'One column with inline popOver', 'roots' ),
        'categories'  => array( 'columns' ),
        'content'     => '<!-- wp:paragraph -->
<p>Ach, Sonitschka, ich habe hier einen scharfen Schmerz erlebt, auf dem Hof, wo ich spaziere, kommen oft Wagen  vom Militär, voll bepackt mit Säcken oder alten Soldatenröcken und -hemden, oft mit Blutflecken <img src="http://test.marcwright.de/rosa/wp-content/uploads/2020/12/rs-lxmbrg__2800x1600.jpg" alt=""> <strong>https://foobar.de | ein extra text im popOver</strong></p>
<!-- /wp:paragraph -->',
    )
);

register_block_pattern(
    'Mini SOUNDCLOUD Player',
    array(
        'title'       => __( 'Pattern for mini Soundcloud Player', 'roots' ),
        'description' => _x( 'Place me for your favorite Soundcloud Player', 'One column with inline popOver', 'roots' ),
        'categories'  => array( 'Media' ),
        'content'     => '<!-- wp:html -->
<iframe class="mb-16" width="100%" height="20" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/964036729&color=%23000000&inverse=false&auto_play=false&show_user=true"></iframe>
<!-- /wp:html -->',
    )
);



register_block_pattern(
    'Petersburg Hanging',
    array(
        'title'       => __( 'A Pattern to show Images in columns with popOver', 'roots' ),
        'description' => _x( 'Place me for Petersburg Hanging', 'Two Columns with popOvers', 'roots' ),
        'categories'  => array( 'columns' ),
        'content'     => '<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"className":"is-style\u002d\u002dflex-wrap"} -->
<div class="wp-block-column is-style--flex-wrap"><!-- wp:image {"id":565,"width":189,"height":330,"sizeSlug":"large","className":"is-style\u002d\u002dxs\u002d\u002dmr"} -->
<figure class="wp-block-image size-large is-resized is-style--xs--mr"><img src="http://test.marcwright.de/rosa/wp-content/uploads/2020/12/rs-lxmbrg__1600x2800.jpg" alt="" class="wp-image-565" width="189" height="330"/><figcaption>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum dolor sit amet exercitation ullamco 500 Z.</figcaption></figure>
<!-- /wp:image -->

<!-- wp:image {"id":566,"sizeSlug":"large","className":"is-style\u002d\u002dxs"} -->
<figure class="wp-block-image size-large is-style--xs"><img src="http://test.marcwright.de/rosa/wp-content/uploads/2020/12/rs-lxmbrg__2800x2800.jpg" alt="" class="wp-image-566"/><figcaption>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum dolor sit amet exercitation ullamco 500 Z.</figcaption></figure>
<!-- /wp:image -->

<!-- wp:image {"id":567,"sizeSlug":"large","className":"is-style\u002d\u002dxs\u002d\u002dml"} -->
<figure class="wp-block-image size-large is-style--xs--ml"><img src="http://test.marcwright.de/rosa/wp-content/uploads/2020/12/rs-lxmbrg__2800x1600.jpg" alt="" class="wp-image-567"/><figcaption>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum dolor sit amet exercitation ullamco 500 Z.</figcaption></figure>
<!-- /wp:image --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->',
    )
);


register_block_pattern(
    '20/80 Text',
    array(
        'title'       => __( 'A Pattern for text 20/ 80', 'roots' ),
        'description' => _x( 'Place me for for a text section', 'Two Columns with text', 'roots' ),
        'categories'  => array( 'columns' ),
        'content'     => '<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":20} -->
<div class="wp-block-column" style="flex-basis:20%"></div>
<!-- /wp:column -->

<!-- wp:column {"width":80} -->
<div class="wp-block-column" style="flex-basis:80%"><!-- wp:paragraph -->
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->',
    )
);
