<?php

    /**
     * Theme filters.
     */

    namespace App;

    /**
     * Add "… Continued" to the excerpt.
     *
     * @return string
     */
    add_filter('excerpt_more', function () {
        //return ' &hellip; <p><a class="button" href="' . get_permalink() . '"><span> ' . __('Continued', 'sage') . '</span><i class="ml-4 inline-block fas fa-arrow-right"></i></a></p>';
    });


    function disable_emoji_feature() {

        // Prevent Emoji from loading on the front-end
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');

        // Remove from admin area also
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('admin_print_styles', 'print_emoji_styles');

        // Remove from RSS feeds also
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');

        // Remove from Embeds
        remove_filter('embed_head', 'print_emoji_detection_script');

        // Remove from emails
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');

        // Disable from TinyMCE editor. Currently disabled in block editor by default
        //add_filter( 'tiny_mce_plugins', __NAMESPACE__ . '\\disable_emojis_tinymce' );

        /** Finally, prevent character conversion too
         ** without this, emojis still work
         ** if it is available on the user's device
         */

        //add_filter( 'option_use_smilies', __NAMESPACE__ . '\\__return_false' );

    }

    function disable_emojis_tinymce($plugins) {
        if (is_array($plugins)) {
            $plugins = array_diff($plugins, array('wpemoji'));
        }

        return $plugins;
    }

    add_action('init', __NAMESPACE__ . '\\disable_emoji_feature');
//add_filter( 'option_use_smilies', __NAMESPACE__ . '\\__return_false' );


// remove generator
    remove_action('wp_head', 'wp_generator');


// remove jquery migrate
    add_action('wp_default_scripts', function ($scripts) {
        if (!empty($scripts->registered['jquery'])) {
            $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, array('jquery-migrate'));
        }
    });

//    /* disable jquery on frontend */
//    function jquery_remove() {
//        if (!is_admin()) {
//            wp_deregister_script('jquery');
//            wp_register_script('jquery', false);
//        }
//    }
//
//    add_action('init', __NAMESPACE__ . '\\jquery_remove');


// enable svg upload
    function cc_mime_types($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    add_filter('upload_mimes', __NAMESPACE__ . '\\cc_mime_types');

// custom file rename function;
    add_filter('mfrh_new_filename', __NAMESPACE__ . '\\my_filter_filename', 10, 3);

    function my_filter_filename($new, $old, $post) {
        return "rs-lxmbrg__" . $new;
    }

    function turn_tagblog_translation_off($taxonomies, $is_settings) {
        unset($taxonomies['post_tag']);

        return $taxonomies;
    }

    add_filter('pll_get_taxonomies', __NAMESPACE__ . '\\turn_tagblog_translation_off', 99999, 2);

    /*function turn_categoryblog_translation_off( $taxonomies, $is_settings ) {
        unset( $taxonomies['category'] );

        return $taxonomies;
    }

    add_filter( 'pll_get_taxonomies', __NAMESPACE__ . '\\turn_categoryblog_translation_off', 99999, 2 );
    */


    function sage_add_category_to_single($classes) {
        if (is_single()) {
            global $post;
            foreach ((get_the_category($post->ID)) as $category) {
                // add category slug to the $classes array
                $classes[] = $category->category_nicename;
            }
        }

        // return the $classes array
        return $classes;
    }

    add_filter('body_class', __NAMESPACE__ . '\\sage_add_category_to_single');


    function sage_remove_category_prefix_from_archive_title($title) {
        if (is_category()) {
            $title = single_cat_title('', false);

        } elseif (is_tag()) {
            $title = single_tag_title('', false);

        } elseif (is_author()) {
            $title = '<span class="vcard">' . get_the_author() . '</span>';

        } elseif (is_archive()) {
            $title = post_type_archive_title('', false);
        }

        return $title;
    }

    add_filter('get_the_archive_title', __NAMESPACE__ . '\\sage_remove_category_prefix_from_archive_title');


    function disable_editor_fullscreen_by_default() {
        $script = "window.onload = function() { const isFullscreenMode = wp.data.select( 'core/edit-post' ).isFeatureActive( 'fullscreenMode' ); if ( isFullscreenMode ) { wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fullscreenMode' ); } }";
        wp_add_inline_script('wp-blocks', $script);
    }

    add_action('enqueue_block_editor_assets', __NAMESPACE__ . '\\disable_editor_fullscreen_by_default');


    function linkFootnotes($content) {
        if (!is_archive()) {

            $string = $content;
            $pattern = '#\[+\d+\]#i';
            $replacement = '<sup><a data-id="#$0" data-role="jump" class="no-underline whitespace-nowrap cursor-pointer hover:opacity-100 ml-1 mr-0.5"><span class="border border-black border-t-0 border-r-0 border-l-0 pointer-events-none">$0</span></a></sup>';

            $content = preg_replace($pattern, $replacement, $string);
            $content = str_replace('a data-id="#[', 'a data-id="#', $content);
            $content = str_replace(']" data-role="jump" ', '" data-role="jump"', $content);
        }

        return $content;
    }

    add_filter('the_content', __NAMESPACE__ . '\\linkFootnotes');


    /**
     * Manage privacy policy so editor and administrator ca edit privacy policy
     */

    function custom_manage_privacy_options($caps, $cap, $user_id, $args) {
        if (!is_user_logged_in()) return $caps;

        $user_meta = get_userdata($user_id);
        if (array_intersect(['editor', 'administrator'], $user_meta->roles)) {
            if ('manage_privacy_options' === $cap) {
                $manage_name = is_multisite() ? 'manage_network' : 'manage_options';
                $caps = array_diff($caps, [$manage_name]);
            }
        }
        return $caps;
    }

    if (is_user_logged_in()) {
        add_action('map_meta_cap', __NAMESPACE__ . '\\custom_manage_privacy_options', 1, 4);
    }

