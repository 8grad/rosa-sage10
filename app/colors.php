<?php

/**
 * Theme admin.
 */

namespace App;

use WP_Customize_Manager;

use function Roots\asset;

/**
 * Register the `.brand` selector to the blogname.
 *
 * @param  \WP_Customize_Manager $wp_customize
 * @return void
 */
add_action('customize_register', function (WP_Customize_Manager $wp_customize) {
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Register the customizer assets.
 *
 * @return void
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset('scripts/customizer.js')->uri(), ['customize-preview'], null, true);
});


/**
 * remove Typo settings
 */

//add_theme_support('editor-font-sizes');


/**
 * Setup colors
 */

function wpdc_add_custom_gutenberg_color_palette() {
    add_theme_support(
        'editor-color-palette',
        [
            [
                'name'  => esc_html__( 'Green', 'wpdc' ),
                'slug'  => 'green',
                'color' => '#47BE61',
            ],
            [
                'name'  => esc_html__( 'Blue', 'wpdc' ),
                'slug'  => 'blue',
                'color' => '#0076E2',
            ],
            [
                'name'  => esc_html__( 'Rose', 'wpdc' ),
                'slug'  => 'rose',
                'color' => '#FAE4E9',
            ],
            [
                'name'  => esc_html__( 'Red', 'wpdc' ),
                'slug'  => 'red',
                'color' => '#ED4646',
            ],
            [
                'name'  => esc_html__( 'Yellow', 'wpdc' ),
                'slug'  => 'yellow',
                'color' => '#FDEC51',
            ],
            [
                'name'  => esc_html__( 'Black', 'wpdc' ),
                'slug'  => 'black',
                'color' => '#000000',
            ],

        ]
    );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\wpdc_add_custom_gutenberg_color_palette', 99 );
