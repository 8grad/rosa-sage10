<?php

    /**
     * create custom post types
     */

    function create_post_types_decision() {
        $post_type = 'decisions';
        $labels = array(
            'name' => __('Decisions', 'sage'),
            'singular_name' => __('Decisions', 'sage'),
            'add_new' => __('create new Decision', 'sage'),
            'add_new_item' => __('create Decisions', 'sage'),
            'edit' => __('Edit', 'sage'),
            'edit_item' => __('edit Decision', 'sage'),
            'new_item' => __('new Decision', 'sage'),
            'view' => __('view Decision', 'sage'),
            'view_item' => __('view Decision', 'sage'),
            'search_items' => __('search Decisions', 'sage'),
            'not_found' => __('no Decisions found', 'sage'),
            'not_found_in_trash' => __('no Decision in trash', 'sage'),
            'parent' => __('Parent Decision', 'sage'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'show_in_rest' => true,
            'public' => true,
            'has_archive' => 'decisions',
            'capability_type' => 'post',
            'menu_position' => 6,
            'menu_icon' => 'dashicons-admin-network',
            'supports' => array('editor', 'title', 'thumbnail', 'page-attributes', 'revisions'),
            //'taxonomies'          => array( 'category', 'post_tag' ),
            'query_var' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
        );
        register_post_type($post_type, $args);
    }

    function create_post_types_positions() {
        $post_type = 'positions';
        $labels = array(
            'name' => __('Positions', 'sage'),
            'singular_name' => __('Positions', 'sage'),
            'add_new' => __('create new Position', 'sage'),
            'add_new_item' => __('create Position', 'sage'),
            'edit' => __('Edit', 'sage'),
            'edit_item' => __('edit Position', 'sage'),
            'new_item' => __('new Position', 'sage'),
            'view' => __('view Position', 'sage'),
            'view_item' => __('view Position', 'sage'),
            'search_items' => __('search Positions', 'sage'),
            'not_found' => __('no Positions found', 'sage'),
            'not_found_in_trash' => __('no Position in trash', 'sage'),
            'parent' => __('Parent Position', 'sage'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'show_in_rest' => true,
            'public' => true,
            'has_archive' => 'positions',
            'capability_type' => 'post',
            'menu_position' => 5,
            'menu_icon' => 'dashicons-welcome-learn-more',
            'supports' => array('editor', 'title', 'thumbnail', 'page-attributes', 'revisions'),
            'query_var' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
        );

        register_post_type($post_type, $args);
    }

    function create_post_types_material() {
        $post_type = 'material';
        $labels = array(
            'name' => __('Material', 'sage'),
            'singular_name' => __('Material', 'sage'),
            'add_new' => __('create new Material', 'sage'),
            'add_new_item' => __('create Material', 'sage'),
            'edit' => __('Edit', 'sage'),
            'edit_item' => __('edit Material', 'sage'),
            'new_item' => __('new Material', 'sage'),
            'view' => __('view Material', 'sage'),
            'view_item' => __('view Material', 'sage'),
            'search_items' => __('search Material', 'sage'),
            'not_found' => __('no Material found', 'sage'),
            'not_found_in_trash' => __('no Material in trash', 'sage'),
            'parent' => __('Parent Material', 'sage'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'show_in_rest' => true,
            'public' => true,
            'has_archive' => 'material',
            'capability_type' => 'post',
            'menu_position' => 9,
            'menu_icon' => 'dashicons-welcome-widgets-menus',
            'supports' => array('editor', 'title', 'excerpt', 'page-attributes'),
            'taxonomies' => array('category'),
            'query_var' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
        );

        register_post_type($post_type, $args);
    }

    function create_post_types_notifications() {
        $post_type = 'notifications';
        $labels = array(
            'name' => __('Notifications', 'sage'),
            'singular_name' => __('Notification', 'sage'),
            'add_new' => __('create new Notification', 'sage'),
            'add_new_item' => __('create Notification', 'sage'),
            'edit' => __('Edit', 'sage'),
            'edit_item' => __('edit Notification', 'sage'),
            'new_item' => __('new Notification', 'sage'),
            'view' => __('view Notification', 'sage'),
            'view_item' => __('view Notification', 'sage'),
            'search_items' => __('search Notification', 'sage'),
            'not_found' => __('no Notification found', 'sage'),
            'not_found_in_trash' => __('no Notification in trash', 'sage'),
            'parent' => __('Parent Notification', 'sage'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => false,
            'show_in_rest' => true,
            'public' => true,
            'capability_type' => 'post',
            'menu_position' => 9,
            'menu_icon' => 'dashicons-megaphone',
            'supports' => array('title'),
            'query_var' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
        );

        register_post_type($post_type, $args);
    }


    add_action('init', __NAMESPACE__ . '\\create_post_types_material', 0);
    add_action('init', __NAMESPACE__ . '\\create_post_types_decision', 0);
    add_action('init', __NAMESPACE__ . '\\create_post_types_positions', 0);
    add_action('init', __NAMESPACE__ . '\\create_post_types_notifications', 0);
