<?php

    /**
     * Theme helpers.
     */

    namespace App;

    function materialCategories($id) {

        $slug = 'citations';
        $citationsObject = get_category_by_slug($slug);
        $citationsId = $citationsObject->term_id;
        $exclude = array($citationsId);

        $args = array(
            'type' => 'material',
            'child_of' => $id,
            'parent' => '',
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => 1,
            'hierarchical' => 1,
            'exclude' => $exclude,
            'include' => '',
            'number' => '',
            'taxonomy' => 'category',
            'pad_counts' => false,
        );
        $categories = get_categories($args);

        echo '<div class="px-3 md:px-0 col-span-12 md:col-span-8 md:col-start-4 md:mb-8">';
        echo '<ul class="categories-list mb-10 flex flex-wrap font-display list-none">';
        foreach ($categories as $category) {
            if (strtolower(get_the_archive_title()) == strtolower($category->name)) {
                $activeClass = "is-active";
            } else {
                $activeClass = "";
            }
            echo "<li class='" . strtolower($category->name) . " " . $activeClass . " mr-2 mb-2'><a class='button " . $activeClass . "' href='" . get_term_link($category->term_id) . "'><span>" . $category->name . " (" . $category->count . ")</span></a></li>";
        }
        echo "</ul>";
        echo "</div>";

    }

    function randomBackground() {
        if (is_front_page()) {
            $themePath = get_template_directory();
            $tailwindConfig = "tailwind.config.php";

            if (!file_exists($tailwind = $themePath . '/' . $tailwindConfig)) {
                wp_die(__('Error locating tailwind.config.php', 'sage'));
            }

            $tailwindCSS = require $tailwind;

            foreach ($tailwindCSS->theme as $section => $value) {
                foreach ($tailwindCSS->theme->colors as $key => $value) {
                    $colorArray[] = $value;
                }
            };

            $randomIndex = rand(0, 5);

            echo "style='background-color: " . $colorArray[$randomIndex] . "' ";
        }
    }


