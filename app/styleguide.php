<?php

/**
 * Theme helpers.
 */

namespace App;

function mapAttNames($section) {
    switch ($section) {
        case 'lineHeight';
            $classPrefix = 'leading';
            break;

        case 'fontSize';
            $classPrefix = 'text';
            break;

        case 'letterSpacing';
            $classPrefix = 'tracking';
            break;

        case 'fontFamily';
            $classPrefix = 'font';
            break;

        case 'colors';
        case 'screens';
        case 'spacing';
            $classPrefix = '*';
            break;

        case 'borderRadius';
            $classPrefix = 'rounded';
            break;

        case 'borderWidth';
            $classPrefix = 'border';
            break;

        case 'boxShadow';
            $classPrefix = 'shadow';
            break;
    }

    return $classPrefix;
}

function styleguide() {
    $themePath = get_template_directory();
    $tailwindConfig = 'tailwind.config.php';

    if (!file_exists($tailwind = $themePath . '/' . $tailwindConfig)) {
        wp_die(__('Error locating tailwind.config.php', 'sage'));
    }

    $blindWord = 'Hamburgefonstiv';
    $blindSentence = "Hillimilli<a href='#'>hirtzi</a>&shy;<i>heftpflaster</i>&shy;<b>Entferner</b>&shy;<br>!\"#$%&'()*+,-./0123456789:; <br><=>?@ABC DEF GHI JKL MNOP QRST UVWXYZ[\] ^ _`abcdefgh";
    $blindBlock = "<p>Manchmal benutzt man Worte wie Hamburgefonts, Rafgenduks oder Handgloves, um Schriften zu testen. Manchmal <i>Sätze</i>, die alle <b>Buchstaben</b> des Alphabets enthalten – man nennt diese Sätze »<a href='https://de.wikipedia.org/wiki/Pangramm'>Pangrams</a>«. Sehr bekannt ist dieser: The quick brown fox jumps over the lazy old dog. Oft werden in Typoblindtexte auch fremdsprachige Satzteile eingebaut (AVAIL® and Wefox™ are testing aussi la Kerning), um die Wirkung in anderen Sprachen zu testen. In Lateinisch sieht zum Beispiel fast jede Schrift gut aus. Quod erat demonstrandum. Seit 1975 fehlen in den meisten Testtexten die Zahlen, weswegen nach TypoGb. 204 § ab dem Jahr 2034 Zahlen in 86 der Texte zur Pflicht werden. Nichteinhaltung wird mit bis zu 245 € oder 368 $ bestraft. Genauso wichtig in sind mittlerweile auch Âçcèñtë, die in neueren Schriften aber fast immer enthalten sind. Ein wichtiges aber schwierig zu integrierendes Feld sind OpenType-Funktionalitäten. Je nach Software und Voreinstellungen können eingebaute Kapitälchen, Kerning oder Ligaturen (sehr pfiffig) nicht richtig dargestellt werden.Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen. Manchmal benutzt man Worte wie Hamburgefonts, Rafgenduks...</p><p>Ein Pangramm (von altgriechisch πᾶν γράμμα pan gramma, deutsch ‚jeder Buchstabe‘) oder holoalphabetischer Satz ist ein Satz, der alle Buchstaben des Alphabets enthält.
Als echt werden Pangramme bezeichnet, in denen jeder Buchstabe genau einmal vorkommt, die also gleichzeitig Isogramme <sup><a href='https://de.wikipedia.org/wiki/Isogramm'>[1]</a></sup> sind. Echte Pangramme mit den 26 lateinischen Buchstaben sind sehr schwer zu erzielen, weil darin nur fünf (oder mit Y sechs) Vokale enthalten sind. Es gibt keine Sprache, für die eines bekannt ist, das nur aus Wörtern des tatsächlichen Sprachgebrauchs ohne Abkürzungen besteht.</p><p>Панграмма (с греч. — «все буквы»), или разнобуквица — короткий текст, использующий все или почти все буквы алфавита, по возможности не повторяя их.</p>";
    $tailwindCSS = require $tailwind;
    $visibleSection = array(
        'lineHeight',
        'fontSize',
        'letterSpacing',
        'fontFamily',
        'screens',
        'colors',
        'spacing',
        'borderWidth',
        'borderRadius',
        'boxShadow',
    );

    echo "<h2 class='mt-32'>Basics from «" . $tailwindConfig . '»</h2>';
    echo "<h5>make sure it is up-to-date! run sync: <code class='bg-gray-400 px-1'>npm run tw</code></h5>";

    echo "<fieldset class='mb-4 mt-12'>";
    echo "<legend class='h3 mb-2'>Paragraph:</legend>";
    echo $blindBlock;
    echo '</fieldset >';

    foreach ($tailwindCSS->theme as $section => $value) {
        if (in_array($section, $visibleSection)) {
            echo "<fieldset id='" . $section . "' class='mt-4 mb-4 p-2'>";
            echo "<legend class='h3 mb-2'>" . ucfirst($section) . ': </legend>';
            foreach ($tailwindCSS->theme->$section as $key => $value) {

                echo "<div class='flex flex-col md:flex-row'>";
                echo "<div class='flex-1 flex'>";
                echo "<div class='flex flex-1'>";

                if (!is_object($value)) {
                    $hasRem = strrpos($value, 'rem');
                }

                echo "<div class='flex-1 flex items-center'><span class='inline-flex text-xs leading-5 font-sans whitespace-no-wrap'><pre><b>" . mapAttNames($section) . "</b>-$key</pre></span></div>";
                echo "<div class='flex-1 flex items-center'>";

                if (is_array($value)) {
                    echo "<span class='bg-blue rounded-xl font-sans text-xs p-1 mr-1 truncate text-white whitespace-no-wrap'>" . $value . '</span>';
                    foreach ($value as $subkey => $subvalue) {
                        echo "<span class='bg-red rounded-xl font-sans text-xs p-1 mr-1 truncate text-white whitespace-no-wrap'>" . $subvalue . '</span> ';
                    }
                }

                if (!is_object($value)) {
                    $hasRem = strrpos($value, 'rem');
                }

                echo "<div class=' flex '>";
                echo "<div class='text-xs font-mono whitespace-pre-line'>";

                if (is_string($value)) {
                    echo $value;
                }

                if (is_object($value)) {
                    foreach ($value as $subkey => $subvalue) {
                        echo '-' . $subkey . ' ' . $subvalue . '<br />';
                    }
                    echo '<br />';
                }

                //print_r($value);
                echo '</div>';

                if ($hasRem) {

                    echo "<span class='text-xs truncate font-mono whitespace-no-wrap'> / ";
                    echo $value * 16;
                    echo 'px ';
                    echo '</span>';
                }
                echo '</div>';

                echo '</div>';
                echo '</div></div>';

                if ($value != '') {
                    switch ($section) {

                        case 'letterSpacing':
                            echo "<div class='flex flex-1 items-center tracking-$key'><span>$blindWord</span></div>";
                            break;

                        case 'fontSize':
                            echo "<div class='flex-1 w-full p-1 overflow-hidden text-$key'><div class='font-display overflow-hidden truncate w-full max-w-full' style='width: 30vw;'>hamburg&shy;efonts</div></div>";
                            break;

                        case 'lineHeight':
                            echo "<div class='flex-1 p-1 overflow-hidden w-full overflow-y-auto overflow-hidden max-w-full break-all leading-$key'>$blindSentence</div>";
                            break;

                        case 'fontFamily':
                            echo "<div class='flex-1 p-1 overflow-hidden font-$key'><div class='overflow-hidden w-full break-all max-w-full' style='width: 40vw;'$blindWord <br />$blindSentence</div></div>";
                            break;

                        case 'spacing':
                            echo "<div class='flex-1 flex justify-center align-center p-1'><span class='bg-rose flex justify-center font-mono items-center text-xs block w-$key h-$key '>$key rem</span></div>";
                            break;

                        case 'borderWidth':
                            echo "<div class='flex-1 flex justify-center p-1 border-$key border-gray-600 inline border-r-0 border-t-0 border-b-0 '><span class=''>$key</span></div>";
                            break;

                        case 'borderRadius':
                            echo "<div class='flex-1 flex justify-center items-center font-mono text-xs p-1'><span class='flex justify-center items-center border w-8 h-8 border-gray-300 rounded-$key'>$key</span></div>";
                            break;

                        case 'boxShadow':
                            echo "<div class='flex-1 flex justify-end items-end font-mono text-xs p-1'><span class='flex justify-center items-center border border-gray-300 w-16 h-16 border-gray-300 rounded-$key shadow-$key'>$key</span></div>";
                            break;

                        // color
                        default:
                            echo "<div class='flex flex-row flex-1 font-mono'>";
                            echo "<div class='flex justify-center flex-col flex-1 w-50 p-1 text-xs bg-$key'>";

                            if (is_string($value)) {
                                echo $value;
                            }

                            if (is_object($value)) {
                                foreach ($value as $subkey => $subvalue) {
                                    echo "<div class='flex-auto font-mono text-xs bg-$key-$subkey'>$subvalue</div>";
                                }
                            }

                            echo '</div>';
                            echo '</div>';
                            break;
                    }
                }
                echo '</div>';
            }
            echo '</fieldset>';
        }
    }
}
