<?php

use function Roots\asset;
//namespace App;

/**
 * Custom Login Logo
 * */
function my_login_logo() {
    $file = asset( 'images/icon.svg' )
    ?>

    <style type="text/css">
        #login h1 a,
        .login h1 a {
            cursor: default;
            pointer-events: none;
            color: #000;
            background-image: url("<?= $file ?>");
            height: 169px;
            width: 239px;
            background-size: auto 160px;
            background-repeat: no-repeat;
            background-position: bottom center;
            margin-bottom: 0;
        }

        #login h1::after,
        .login h1::after {
            content: "";
            top: -1rem;
            position: relative;
        }

        #login .button-primary {
            background-color: #ed4646 !important;
            border-color: #ed4646 !important;
        }
        #login input:focus {
            border-color: #ed4646 !important;
            box-shadow: 0 0 0 1px #ed4646 !important;
        }

        #login .button-secondary,
        #login a {
            color: #ed4646 !important;
        }
        #login .message {
            border-color: #ed4646 !important;
        }
    </style>
<?php }

add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo' );


/**
 * Enqueues theme styles for the editor.
 */
add_action( 'after_setup_theme', function() {

    // Add support for editor styles.
    add_theme_support( 'editor-styles' );

    // Enqueue block editor stylesheet.
    add_editor_style( 'dist/styles/editor.css' );
});
