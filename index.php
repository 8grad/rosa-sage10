<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php

        use function Roots\asset;

    ?>

    <link rel="icon" type="image/png" sizes="192x192" href="<?= asset( 'images/android-icon-192x192.png' ) ?>"/>
    <link rel="icon" type="image/png" sizes="32x32" href="<?= asset( 'images/favicon-32x32.png' ) ?>"/>
    <link rel="icon" type="image/png" sizes="96x96" href="<?= asset( 'images/favicon-96x96.png' ) ?>"/>
    <link rel="icon" type="image/png" sizes="16x16" href="<?= asset( 'images/favicon-16x16.png' ) ?>"/>

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= asset( 'images/ms-icon-144x144.png' ) ?>"/>
    <meta name="theme-color" content="#000000">

    <meta name="referrer" content="no-referrer-when-downgrade"/>

    <meta name="theme-color" content="#ffffff">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="manifest" href="<?= asset( 'images/manifest.json' ) ?>"/>
    <link rel="apple-touch-icon" href="<?= asset( 'images/apple-icon-180x180--white.png' ) ?>">

    <meta name="description" content="<?= __( 'Eine Webseite zu Positionen von Rosa Luxemburg', 'sage' ) ?>">

    <link rel="dns-prefetch" href="//via.placeholder.com"/>
    <link rel="preconnect" href="//via.placeholder.com"/>

    <link rel="dns-prefetch" href="//unpkg.com"/>
    <link rel="preconnect" href="//unpkg.com"/>

    <link rel='dns-prefetch' href='//fonts.typotheque.com'/>
    <link rel='preconnect' href='//fonts.typotheque.com'/>


    <link rel="preload"
          href="<?= asset( 'fonts/NoeText-Regular.woff' ) ?>"
          as="font"
          type="font/woff"
          crossorigin="anonymous"/>

    <link rel="preload"
          href="<?= asset( 'fonts/NoeText-Regular.woff2' ) ?>"
          as="font"
          type="font/woff2"
          crossorigin="anonymous"/>

    <link rel="preload"
          href="<?= asset( 'fonts/NoeDisplay-Bold.woff' ) ?>"
          as="font"
          type="font/woff"
          crossorigin="anonymous"/>

    <link rel="preload"
          href="<?= asset( 'fonts/NoeDisplay-Bold.woff2' ) ?>"
          as="font"
          type="font/woff2"
          crossorigin="anonymous"/>

    <link rel="preload"
          href="<?= asset( 'fonts/Silka-Bold.woff' ) ?>"
          as="font"
          type="font/woff"
          crossorigin="anonymous"/>

    <link rel="preload"
          href="<?= asset( 'fonts/Silka-Bold.woff' ) ?>"
          as="font"
          type="font/woff2"
          crossorigin="anonymous"/>

    <link rel="preload"
          href="<?= asset( 'fonts/Silka-Light.woff2' ) ?>"
          as="font"
          type="font/woff2"
          crossorigin="anonymous"/>


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Hosted Webfont from typotheque -->
    <link rel=“stylesheet” href="https://fonts.typotheque.com/WF-037286-011749.css" type="text/css"/>

    <?php
        $postType    = get_post_type( get_the_ID(), true );
        $metaOptions = get_field( $postType, 'options' );
    ?>

    <!-- meta -->
    <meta property="og:title" content="<?= $metaOptions["title"] ?>"/>
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?= get_permalink(); ?>">
    <meta property="og:description" content="<?= $metaOptions["description"] ?>"/>
    <meta name="description" content="<?= $metaOptions["description"] ?>"/>
    <meta name="robots" content="index,follow">

    <!-- √ meta -->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-url="<?= site_url(); ?>" data-more="<?= __( 'Continued', 'sage' ) ?>">
<noscript>
    <p
        class="bg-red p-2 z-10 fixed w-full font-bold"><?= __( 'Sorry, this site requires Javascript and a modern browser.', 'sage' ) ?></p>
</noscript>
<p
    class="browser-support bg-red p-2 z-10 fixed w-full bottom-0 font-bold"><?= __( 'Sorry, this site requires a modern browser.', 'sage' ) ?></p>

<?php wp_body_open(); ?>
<?php do_action( 'get_header' ); ?>

<div id="app" class="font-serif">
    <?php echo \Roots\view( \Roots\app( 'sage.view' ), \Roots\app( 'sage.data' ) )->render(); ?>
</div>

<?php do_action( 'get_footer' ); ?>
<?php wp_footer(); ?>
</body>
</html>
